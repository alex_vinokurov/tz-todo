# Тестовое задание TODO приложение

### Запуск приложения (в режиме HMR)
1. Клонировать репозиторий
2. Выполнить `npm install && npm start`. Дождаться сообщения о сборке Webpack-ом в консоли.
3. Открыть `localhost:8080` в браузере

### Бэкенд
В приложении используется бэкенд на Node.js, предоставляющий REST API. Путь: src/backend. Данные записываются в файл data/tasks.json.
Бэкенд запускается скриптом start параллельно с фронтендом, использует порт 8085, отдельного запуска или конфигурации не требует.
